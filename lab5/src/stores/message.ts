import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useMessagerStore = defineStore("message", () => {
  const isShow = ref(false);
  const message = ref("");
  const showMessage = (msg: string) => {
    message.value = msg;
    isShow.value = true;
  };
  const closeMessage = () => {
    message.value = "";
    isShow.value = false;
  };

  return { isShow, message, showMessage, closeMessage };
});
